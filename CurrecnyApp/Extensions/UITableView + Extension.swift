//
//  UITableView + Extension.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 06/08/2021.
//

import Foundation
import UIKit

extension UITableView {
    func registerForCells(strIDs : [String]) {
        strIDs.forEach { (strID) in
            register(UINib(nibName: strID, bundle: nil), forCellReuseIdentifier: strID)
        }
    }
}
