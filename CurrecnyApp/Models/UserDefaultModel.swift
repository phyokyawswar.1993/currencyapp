//
//  UserDefaultModel.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 10/08/2021.
//

import Foundation

class UserDefaultModel {
    static let shared = UserDefaultModel()
    private init() {
        
    }
}

extension UserDefaultModel {
    func saveData(data : Data , key : String) {
        UserDefaults.standard.set(data, forKey: key)
    }
    
    func retrieveData(key : String) -> Data? {
        return UserDefaults.standard.data(forKey: key)
    }
    
    func saveString(value : String , key : String) {
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func retrieveString(key : String) -> String? {
        return UserDefaults.standard.string(forKey: key)
    }
}
