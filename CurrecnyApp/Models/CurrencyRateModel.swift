//
//  CurrencyRateModel.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 06/08/2021.
//

import Foundation
import Combine

enum Keys : String{
    case updatedTime = "UpdatedTime"
    case exchangeRate = "ExchangeRate"
    case currencies = "Currencies"
    
    func getKey() -> String {
        return self.rawValue
    }
}
struct CurrencyRateModel {
    static let shared = CurrencyRateModel()
    
    func getCurrencyExchangeRateList(source : String) -> AnyPublisher<CurrencyRateResponse?,Error> {
        let url = ApiConfig.Home.getCurrencyExchangeRateList(source).getUrlString()
        
        return ApiClient.shared.requestCombine(url: url).compactMap
        { data -> CurrencyRateResponse? in
            
            let timestamp = NSDate().timeIntervalSince1970
            UserDefaultModel.shared.saveString(value: "\(timestamp)", key: Keys.updatedTime.getKey())
            UserDefaultModel.shared.saveData(data: data, key: Keys.exchangeRate.getKey())
            if let currencyRateResponse = data.decode(modelType: CurrencyRateResponse.self){
                return currencyRateResponse
            }
            return nil
        }.eraseToAnyPublisher()
    }
    
    func getCurrencyList() -> AnyPublisher<CurrencyListResponse?,Error>{
        let url = ApiConfig.Home.getCurrencyList.getUrlString()
        
        return ApiClient.shared.requestCombine(url: url).compactMap
        { data -> CurrencyListResponse? in
            UserDefaultModel.shared.saveData(data: data, key: Keys.currencies.getKey())
            if let currencyResponse = data.decode(modelType: CurrencyListResponse.self){
                return currencyResponse
            }
            return nil
        }.eraseToAnyPublisher()
    }
    
  
}


