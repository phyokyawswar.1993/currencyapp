//
//  CalculatorViewController.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 08/08/2021.
//

import UIKit
import ActionSheetPicker_3_0
class CalculatorViewController: BaseViewController {
    
    @IBOutlet weak var fromCalculatorView: CalculateExchangeRateView!
    @IBOutlet weak var toCalculatorView: CalculateExchangeRateView!
    
    var viewModel = CalculatorViewModel()
    var selectedCurrency = String()
    var keyArray = [String]()
    var valueArray = [String]()
    var fromValue = Double()
    var exchangeRateArray = [[String : Double?]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(exchangeRateArray)
        viewModel.selectedFromCurrencyPublisher.send(selectedCurrency)
        viewModel.selectedToCurrencyPublisher.send(keyArray.first ?? "USD")
        
        self.calculateToValue()
    }
    
    override func setupUI() {
        super.setupUI()
        self.title = "Calculate Rate"
        fromCalculatorView.delegate = self
        toCalculatorView.delegate = self
        
        setupFromCalculatorView()
        setupToCalculatorView()
    }
    
    override func bindData() {
        super.bindData()
        viewModel.selectedToCurrencyPublisher.dropFirst().sink {_ in
            self.toCalculatorView.setupView(viewModel: &self.viewModel, btnType: .toCurrency)
            self.calculateToValue()
        }.store(in: &bindings)
        
        viewModel.selectedFromCurrencyPublisher.dropFirst().sink {_ in
            self.fromCalculatorView.setupView(viewModel: &self.viewModel, btnType: .fromCurrency)
            self.calculateToValue()
        }.store(in: &bindings)
        
        viewModel.fromValuePublisher.dropFirst().sink {
            if !$0.isEmpty{
                self.fromCalculatorView.tfValue.text = $0
                self.fromValue = Double($0) ?? 1.0
                self.calculateToValue()
            }
        }.store(in: &bindings)
        
        viewModel.toValuePublisher.dropFirst().sink {
            self.toCalculatorView.tfValue.text = $0
            
        }.store(in: &bindings)
    }
    
    func calculateToValue() {
        
        switch self.exchangeCurrencyType() {
        case .equalBaseAndFromCurrency:
            let key = "\(self.viewModel.selectedFromCurrencyPublisher.value)\(self.viewModel.selectedToCurrencyPublisher.value)"
            let index = exchangeRateArray.firstIndex{$0.keys.first == key}
            
            if let value = self.exchangeRateArray[index ?? 0][key] {
                
                let calculatedValue = (Double(self.viewModel.fromValuePublisher.value) ?? 1.0 ) * (value ?? 1.0)
                self.viewModel.toValuePublisher.send(String(format: "%.2f",calculatedValue))
            }
        case .equalFromAndToCurrency :
            showAlert(title: "Information", message: "Please select different currency!")
            self.viewModel.selectedFromCurrencyPublisher.send("")
            self.viewModel.selectedToCurrencyPublisher.send("")
        case .notEqualBaseAndFromCurrency :
            print("Calculate")
            let firstKey = "\(self.selectedCurrency)\(self.viewModel.selectedFromCurrencyPublisher.value)"
            let firstIndex = exchangeRateArray.firstIndex{$0.keys.first == firstKey}
            
            let secondKey = "\(self.selectedCurrency)\(self.viewModel.selectedToCurrencyPublisher.value)"
            let secondIndex = exchangeRateArray.firstIndex{$0.keys.first == secondKey}
            
            if let firstValue = self.exchangeRateArray[firstIndex ?? 0][firstKey] ,
               let secondValue = self.exchangeRateArray[secondIndex ?? 0][secondKey]{
                let firstCalculateValue = 1 / (firstValue ?? 1.0)
                let secondCalculateValue = (Double(self.viewModel.fromValuePublisher.value) ?? 1.0) * firstCalculateValue * (secondValue ?? 1.0)
                self.viewModel.toValuePublisher.send(String(format: "%.2f", secondCalculateValue))
            }
        }
        
        
    }
    
    private func exchangeCurrencyType() -> ExchangeCurrencyType {
        if self.selectedCurrency == self.viewModel.selectedFromCurrencyPublisher.value {
            return .equalBaseAndFromCurrency
        }
        else if self.viewModel.selectedFromCurrencyPublisher.value == self.viewModel.selectedToCurrencyPublisher.value {
            return .equalFromAndToCurrency
        }
        else {
            return .notEqualBaseAndFromCurrency
        }
        
    }
    
    private func setupFromCalculatorView() {
        fromCalculatorView.btnCurrency.titleLabel?.font = .boldSystemFont(ofSize: 15.0)
        fromCalculatorView.btnCurrency.setTitle(selectedCurrency, for: .normal)
        fromCalculatorView.tfValue.text = "1"
    }
    
    private func setupToCalculatorView() {
        toCalculatorView.btnCurrency.titleLabel?.font = .boldSystemFont(ofSize: 15.0)
        toCalculatorView.btnCurrency.setTitle(keyArray.first, for: .normal)
        
    }
}

extension CalculatorViewController : CalculateExchangeRateViewDelegate {
    func didTapCurrencyBtn(btnType: CurrencyBtnType) {
        var initialSelectedIndex : Int?
        switch btnType {
        case .fromCurrency:
            initialSelectedIndex = keyArray.firstIndex{$0 == viewModel.selectedFromCurrencyPublisher.value }
        case .toCurrency:
            initialSelectedIndex = keyArray.firstIndex{$0 == viewModel.selectedToCurrencyPublisher.value }
        }
        ActionSheetStringPicker.show(withTitle: "Select currency type", rows: valueArray, initialSelection: initialSelectedIndex ?? 0, doneBlock: { picker, row, _ in
            switch btnType {
            case .fromCurrency :
                self.viewModel.selectedFromCurrencyPublisher.send(self.keyArray[row])
            case .toCurrency :
                self.viewModel.selectedToCurrencyPublisher.send(self.keyArray[row])
            }
        }, cancel: { picker in
            
        }, origin: self.view)
    }
}
