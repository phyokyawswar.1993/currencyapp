//
//  HomeViewModel.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 06/08/2021.
//

import Foundation
import Combine
class HomeViewModel: BaseViewModel {
    let model = CurrencyRateModel.shared
    let currencyReateResponsePublisher = PassthroughSubject<CurrencyRateResponse?,Never>()
    let currencyListResponsePublisher = PassthroughSubject<CurrencyListResponse?,Never>()
    let currencyKeyPublisher = CurrentValueSubject<String,Never>("USD")
  
}

extension HomeViewModel {
    func getCurrencyExchangeRateList(source : String = "USD") {
        self.loadingPublisher.send(true)
        model.getCurrencyExchangeRateList(source: source).sink(receiveCompletion: {
            self.loadingPublisher.send(false)
            guard case .failure(let error) = $0 else {return}
            self.errorPublisher.send(error)
        }){ (response) in
            self.loadingPublisher.send(false)
            self.currencyReateResponsePublisher.send(response)
        }.store(in : &bindings)

    }
    
    func getCurrencyList() {
        self.loadingPublisher.send(true)
        model.getCurrencyList().sink(receiveCompletion: {
            self.loadingPublisher.send(false)
            guard case .failure(let error) = $0 else {return}
            self.errorPublisher.send(error)
        }){ (response) in
            self.loadingPublisher.send(false)
            self.currencyListResponsePublisher.send(response)
        }.store(in : &bindings)
    }
}
