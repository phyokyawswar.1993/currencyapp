//
//  CurrencyTableViewCell.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 06/08/2021.
//

import UIKit

class CurrencyTableViewCell: BaseTableViewCell {

    @IBOutlet weak var lblCurrencyName: UILabel!
    @IBOutlet weak var lblCurrencyValue: UILabel!
    
    override func setupUIs() {
        super.setupUIs()
    }
    
    func setupCell(exchangeRate : [String : Double?]){
        lblCurrencyName.text = exchangeRate.keys.first
        if let value = exchangeRate.values.first {
            lblCurrencyValue.text = String(format: "%.2f", value ?? 0.0)
        }
        else {
            lblCurrencyValue.text = "0.0"
        }
    }
}
