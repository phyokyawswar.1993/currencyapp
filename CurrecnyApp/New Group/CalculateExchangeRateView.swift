//
//  CalculateExchangeRateView.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 08/08/2021.
//

import UIKit

protocol CalculateExchangeRateViewDelegate {
    func didTapCurrencyBtn(btnType : CurrencyBtnType)
}
class CalculateExchangeRateView: BaseView {
    @IBOutlet weak var btnCurrency: UIButton!
    @IBOutlet weak var tfValue: UITextField!
    var currencyBtnType : CurrencyBtnType = .fromCurrency
    var delegate : CalculateExchangeRateViewDelegate?
    var viewModel : CalculatorViewModel?
    
    override func setupUI() {
        super.setupUI()
    }
    
    override func bindData() {
        super.bindData()
        
        btnCurrency.tapPublisher.sink {
            self.delegate?.didTapCurrencyBtn(btnType: self.currencyBtnType)
        }.store(in: &bindings)
        
        tfValue.textPublisher.sink {
            switch self.currencyBtnType {
            case .fromCurrency :
                self.viewModel?.fromValuePublisher.send($0 ?? "1.0")
            case .toCurrency :
                self.viewModel?.toValuePublisher.send($0 ?? "1.0")
            }
        }.store(in: &bindings)
    }
    
    func setupView(viewModel : inout CalculatorViewModel , btnType : CurrencyBtnType) {
        self.currencyBtnType = btnType
        self.viewModel = viewModel
        switch btnType {
        case .fromCurrency:
            btnCurrency.setTitle(viewModel.selectedFromCurrencyPublisher.value, for: .normal)
        case .toCurrency:
            btnCurrency.setTitle(viewModel.selectedToCurrencyPublisher.value, for: .normal)
            tfValue.isUserInteractionEnabled = false
        }
        
        
       
    }

}
