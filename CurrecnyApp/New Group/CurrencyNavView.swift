//
//  CurrencyNavView.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 08/08/2021.
//

import UIKit
import CombineCocoa

protocol CurrencyNavViewDelegate {
    func didTapCurrency()
}
class CurrencyNavView: BaseView {

    @IBOutlet weak var btnCurrencyType: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCurrencyType: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    
    var delegate : CurrencyNavViewDelegate?
    override func setupUI() {
        super.setupUI()
    }
    
    override func bindData() {
        super.bindData()
        btnCurrencyType.tapPublisher.sink {
            self.delegate?.didTapCurrency()
        }.store(in: &bindings)
    }
    
    func setupView(currency : String = "USD") {
        lblTitle.text = "Exchange from : "
        lblCurrencyType.text = currency
    }
}
