//
//  HomeTableViewController.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 06/08/2021.
//

import UIKit
import ActionSheetPicker_3_0
import SnapKit

class HomeTableViewController: BaseTableViewController {

    let viewModel = HomeViewModel()
    var currencyExchangeRateArray : [[String : Double?]]?
    var currencyKeyArray = [String]()
    var currencyValueArray = [String]()
    var btnCalculator = UIButton()
    var titleView : CurrencyNavView?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let rateResponse = UserDefaultModel.shared.retrieveData(key: Keys.exchangeRate.getKey())?.decode(modelType: CurrencyRateResponse.self),
           rateResponse.success == true ,
           let currencyResponse = UserDefaultModel.shared.retrieveData(key: Keys.currencies.getKey())?.decode(modelType: CurrencyListResponse.self) ,
           currencyResponse.success == true {
            self.viewModel.currencyListResponsePublisher.send(currencyResponse)
            self.viewModel.currencyReateResponsePublisher.send(rateResponse)
        }
        else {
            viewModel.getCurrencyExchangeRateList()
            viewModel.getCurrencyList()
        }
       
    }
    
    override func setupUI() {
        super.setupUI()
        setTitleView()
        setCalculatorBtn()
    }
    
    override func bindViewModel() {
        super.bindViewModel()
        viewModel.bindViewModel(in: self)
    }
    
    override func bindData() {
        super.bindData()
        
        viewModel.currencyReateResponsePublisher.sink {
            if let response = $0 ,
               response.success ?? false {
                self.currencyExchangeRateArray = response.quotes?.compactMap{[$0.key : $0.value]}
            }
            else {
                if let error = $0?.error {
                    self.showAlert(title: "Information", message: error.info ?? "")
                }
            }
            self.hidePullToRefreshPublisher.send(false)
            self.tableView.reloadData()
        }.store(in: &bindings)
        
        showPullToRefreshPublisher.sink {
            if $0 {
                if self.canRefresh() {
                    self.currencyExchangeRateArray?.removeAll()
                    self.viewModel.getCurrencyExchangeRateList()
                }
                else {
                    self.showToast(message: "Can't refresh right now , Please try after 30 mins")
                    self.hidePullToRefreshPublisher.send(true)
                }
            }
            
        }.store(in: &bindings)
        viewModel.currencyListResponsePublisher.sink {
            if let response = $0 ,
               response.success ?? false {
                self.currencyKeyArray = response.currencies?.compactMap({$0.key}) ?? []
                self.currencyValueArray = response.currencies?.compactMap({$0.value}) ?? []
            }
        }.store(in: &bindings)
        
       
        viewModel.currencyKeyPublisher.dropFirst().sink {
            print($0)
            self.currencyExchangeRateArray?.removeAll()
            if let view = self.titleView {
                view.setupView(currency: $0)
            }
            self.viewModel.getCurrencyExchangeRateList(source: $0)
        }.store(in: &bindings)
        
        btnCalculator.tapPublisher.sink {
            AppScreen.shared.navigateToCalculatorView(selectedCurrency: self.viewModel.currencyKeyPublisher.value,
                                                      keyArray: self.currencyKeyArray,
                                                      valueArray: self.currencyValueArray,
                                                      exchangeRateArray: self.currencyExchangeRateArray ?? [])
        }.store(in: &bindings)
    }
    
    override func setupTableView() {
        super.setupTableView()
        tableView.registerForCells(strIDs: [String(describing: CurrencyTableViewCell.self)])
        
        setupHeaderLoadingAnimation()
    }
    
    func setTitleView() {
        
        titleView = CurrencyNavView(frame:CGRect(x: (self.view.frame.size.width / 2) - 100, y: 0, width: 200, height: 80))
        titleView?.setupView()
        titleView?.delegate = self
        self.navigationItem.titleView = titleView
    }
    
    
    func setCalculatorBtn() {
        self.view.addSubview(btnCalculator)
        btnCalculator.setBackgroundImage(#imageLiteral(resourceName: "calculator"), for: .normal)
        btnCalculator.backgroundColor = .lightGray
        btnCalculator.layer.cornerRadius = 25
        btnCalculator.clipsToBounds = true
        btnCalculator.snp.makeConstraints { make in
            make.width.equalTo(50)
            make.height.equalTo(50)
            make.bottomMargin.equalTo(-20)
            make.trailingMargin.equalTo(-20)
        }
    }
   
    
    private func canRefresh() -> Bool {
        if let updatedTime = UserDefaultModel.shared.retrieveString(key: Keys.updatedTime.getKey()){
            let updatedTimeStamp = TimeInterval(updatedTime) ?? 0
            let updatedDate = Date(timeIntervalSince1970: updatedTimeStamp)
            
            let timeDifferent = Calendar.current.dateComponents([.minute], from: updatedDate, to: Date())
            
            if (timeDifferent.minute ?? 0) > 30 {
                return true
            }
           
        }
        return false
    }

    
}

extension HomeTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.currencyExchangeRateArray?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CurrencyTableViewCell.self), for: indexPath) as! CurrencyTableViewCell
        if let exchangeRate = self.currencyExchangeRateArray?[indexPath.row] {
            cell.setupCell(exchangeRate: exchangeRate)
        }
        
        return cell
    }
}

extension HomeTableViewController : CurrencyNavViewDelegate {
    func didTapCurrency() {
        if !self.currencyValueArray.isEmpty {
            let initialIndex = self.currencyKeyArray.firstIndex{$0 == self.viewModel.currencyKeyPublisher.value}
            ActionSheetStringPicker.show(withTitle: "Choose currencyType", rows: self.currencyValueArray, initialSelection: initialIndex ?? 0, doneBlock: { picker, row, _ in
                self.viewModel.currencyKeyPublisher.send(self.currencyKeyArray[row])
               
            }, cancel: { picker in
                
            }, origin: self.view)
        
        }
    }
}
