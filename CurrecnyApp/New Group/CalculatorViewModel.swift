//
//  CalculatorViewModel.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 09/08/2021.
//

import Foundation
import Combine

enum CurrencyBtnType {
    case fromCurrency
    case toCurrency
}

enum ExchangeCurrencyType {
    case equalBaseAndFromCurrency
    case notEqualBaseAndFromCurrency
    case equalFromAndToCurrency
}
class CalculatorViewModel: BaseViewModel {
    let selectedFromCurrencyPublisher = CurrentValueSubject<String,Never>("")
    let selectedToCurrencyPublisher = CurrentValueSubject<String,Never>("")
    
    let fromValuePublisher = CurrentValueSubject<String,Never>("1")
    let toValuePublisher = CurrentValueSubject<String,Never>("")
}
