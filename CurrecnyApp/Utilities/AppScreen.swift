//
//  AppScreen.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 06/08/2021.
//

import Foundation
import UIKit
struct AppScreen {
    static var shared = AppScreen()
    var currentVC : UIViewController?
}

extension AppScreen {
    func navigateToCalculatorView(selectedCurrency : String , keyArray : [String] , valueArray : [String] , exchangeRateArray : [[String : Double?]]) {
        let vc = CalculatorViewController.init()
        vc.selectedCurrency = selectedCurrency
        vc.keyArray = keyArray
        vc.valueArray = valueArray
        vc.exchangeRateArray = exchangeRateArray
        currentVC?.navigationController?.pushViewController(vc, animated: true)
    }
}
