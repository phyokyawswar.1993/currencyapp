//
//  ErrorVO.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 08/08/2021.
//

import Foundation

struct ErrorVO : Codable {
    let code : Int?
    let info : String?
}
