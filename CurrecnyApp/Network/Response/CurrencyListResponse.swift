//
//  CurrencyListResponse.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 08/08/2021.
//

import Foundation

struct CurrencyListResponse : Codable {
    let success : Bool?
    let terms : String?
    let privacy : String?
    let currencies : [String : String]?
   
}
