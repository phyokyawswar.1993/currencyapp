//
//  CurrencyRateResponse.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 06/08/2021.
//

import Foundation

struct CurrencyRateResponse:  Codable {
    let success : Bool?
    let terms : String?
    let privacy : String?
    let timestamp : Int32?
    let source : String?
    let quotes : [String : Double?]?
    let error : ErrorVO?
}
