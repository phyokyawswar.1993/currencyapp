//
//  ApiConfig.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 06/08/2021.
//

import Foundation

struct ApiConfig {
    static let baseUrl = "http://api.currencylayer.com/"
    static let AccessKey = "5ed9ac288e3c02bfc39a1ff9ca8a4281"
    
    enum Home {
        case getCurrencyExchangeRateList(_ source: String)
        case getCurrencyList
        
        func getUrlString() -> String {
            switch self {
            case .getCurrencyExchangeRateList(let source):
                return ApiConfig.baseUrl + "live?source=\(source)&access_key=\(ApiConfig.AccessKey)"
            case .getCurrencyList:
                return ApiConfig.baseUrl + "list?access_key=\(ApiConfig.AccessKey)"
            }
        }
    }
    
 
}
