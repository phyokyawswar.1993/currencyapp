//
//  NetworkErrorType.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 06/08/2021.
//

import Foundation

enum NetworkErrorType: Error {
    case NoInterntError
    case KnownError(_ errorMessage: String)
}

