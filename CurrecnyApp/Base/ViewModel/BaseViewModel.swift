//
//  BaseViewModel.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 06/08/2021.
//

import Foundation
import Combine
class BaseViewModel {
    var bindings = Set<AnyCancellable>()
    
    let errorPublisher = PassthroughSubject<Error,Never>()
    
    var viewcontroller : BaseViewController?
    let isNoInternetPublisher = PassthroughSubject<Bool,Never>()
    let showErrorMessagePublisher = CurrentValueSubject<String?,Never>(nil)
    let loadingPublisher = CurrentValueSubject<Bool,Never>(false)
    init() {
        
    }
    deinit {
        debugPrint("Deinit \(type(of: self))")
    }
    func bindViewModel(in viewController: BaseViewController? = nil) {
        self.viewcontroller = viewController
        
        loadingPublisher.sink { (result) in
            if result {
                viewController?.showLoading()
               
            } else {
                viewController?.hideLoading()
               
            }
        
        }.store(in: &bindings)
        
        errorPublisher.sink { (error) in
            viewController?.hideLoading()
            
            if let error = error as? NetworkErrorType {
                switch error {
                case .NoInterntError:
                    self.isNoInternetPublisher.send(true)
                case .KnownError(let message):
                    viewController?.showToast(message: message)
                }
            }
        }.store(in: &bindings)
        
        showErrorMessagePublisher.sink { (errorMessage) in
            if let message = errorMessage {
                viewController?.showToast(message: message, isShowing: {
                    self.viewcontroller?.view.isUserInteractionEnabled = false
                }, completion: {
                    self.viewcontroller?.view.isUserInteractionEnabled = true
                })
                
            }
        }.store(in: &bindings)
    }
    
   
}
