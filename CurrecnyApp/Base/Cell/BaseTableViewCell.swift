//
//  BaseTableViewCell.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 06/08/2021.
//

import UIKit
import Combine
class BaseTableViewCell: UITableViewCell {

    var bindings = Set<AnyCancellable>()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupUIs()
        bindData()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupUIs(){
        selectionStyle = .none
    }
    
    override func prepareForReuse() {
        applyTheme()
        bindings = Set<AnyCancellable>()
        bindData()
    }
    
    func applyTheme(){
        selectionStyle = .none
    }
   
    
    func bindData() {
        
    }
}
