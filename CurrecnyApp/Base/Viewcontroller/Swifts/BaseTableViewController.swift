//
//  BaseTableViewController.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 8/08/2021.
//

import UIKit
import CRRefresh
import Combine
class BaseTableViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    fileprivate let activityIndicator = UIActivityIndicatorView()
    
    let showPullToRefreshPublisher = CurrentValueSubject<Bool,Never>(false)
    let hidePullToRefreshPublisher = CurrentValueSubject<Bool,Never>(true)
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func bindData() {
        super.bindData()
        
        hidePullToRefreshPublisher.sink {_ in
            self.tableView.cr.endHeaderRefresh()
        }.store(in: &bindings)
    }
    
    override func setupUI() {
        super.setupUI()
        //        self.removeNavigationBorder()
        setupTableView()
        
    }
    
    func setupTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.register(UINib.init(nibName: "RefreshCell", bundle: nil), forCellReuseIdentifier: "RefreshCell")
    }
    
    
    func setupHeaderLoadingAnimation() {
        tableView.cr.addHeadRefresh(animator: NormalHeaderAnimator()) { [weak self] in
            self?.showPullToRefreshPublisher.send(true)
        }
    }
    
    
    func registerForCells(identifiers : [String]){
        tableView.registerForCells(strIDs: identifiers)
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get{
            return .portrait
        }
    }
    
    //MARK:- ActivityIndicator
    /* for pull to refresh and load more*/
    func showBottomIndicator(){
        activityIndicator.startAnimating()
        activityIndicator.color = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        activityIndicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0)
                                         , width: tableView.bounds.width, height: CGFloat(44))
        
        self.tableView.tableFooterView = activityIndicator
    }
    
    func hideBottomIndicator(){
        activityIndicator.stopAnimating()
        self.tableView.tableFooterView = UIView()
    }
    
    func showTopIndicator(){
        activityIndicator.startAnimating()
        activityIndicator.color = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        activityIndicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0)
                                         , width: tableView.bounds.width, height: CGFloat(44))
        
        self.tableView.tableHeaderView = activityIndicator
    }
    
    func hideTopIndicator(){
        activityIndicator.stopAnimating()
        self.tableView.tableHeaderView = UIView()
    }
}

//MARK:- Loading Indicator
extension BaseTableViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
    
}
