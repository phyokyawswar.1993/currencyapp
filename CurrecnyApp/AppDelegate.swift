//
//  AppDelegate.swift
//  CurrecnyApp
//
//  Created by Phyo Kyaw Swar on 06/08/2021.
//

import UIKit
import IQKeyboardManagerSwift
@main
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        setRootVC()
        return true
    }
    
    
    private func setRootVC() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let vc = HomeTableViewController(nibName: String(describing: BaseTableViewController.self), bundle: nil)
        let navigationVC = UINavigationController(rootViewController: vc)
        window?.rootViewController = navigationVC
        window?.makeKeyAndVisible()
    }

}

